import Vue from "vue";
import Vuex from "vuex";
import otro from './modules/otro'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    materia: ''
  },
  mutations: {
    setMateria(state, payload){
      state.materia = payload;
    }
  },
  actions: {
    initializateMateria({commit}){
        let materia = 'Español';
        commit('setMateria', materia);
    }
  },
  getters: {},
  modules:{
    otro
  }
});
